/* 
 * Create an object representation of yourself
 * should include firstName, lastName, 'favorite food'
 * should also include a mom object and a dad object with
 * the same properties as above
 */



 // console.log dad's firstName, mom's favorite food


/* 
 * Tic-Tac-Toe
 * Create an array to represent the tic-tac-toe board (see slide)
 */



// After the array is created, 'O' takes the top right square.  Update.



// Log the grid to the console.
// Hint: log each row separately.



/*
 * Validate the email
 * You are given an email as string myEmail
 * make sure it is in correct email format.
 * Should be in this format, no whitespace:
 * 1 or more characters
 * @ sign
 * 1 or more characters
 * .
 * 1 or more characters
 */
const myEmail = 'foo@bar.baz'; // good email
const badEmail = 'badmail@gmail'; // bad email
