const assignmentDate = '1/21/2019';
const dueDate = '1/28/2019';
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

// Convert to a Date instance
// Create dueDate which is 7 days after assignmentDate
const assignDateObject = new Date(assignmentDate);
const dueDateObject = new Date(dueDate);

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
const dueDateString = `<time datetime="${dueDateObject.getFullYear()}-${('0'+(dueDateObject.getMonth()+1)).slice(-2)}-${('0'+(dueDateObject.getDate())).slice(-2)}">${months[dueDateObject.getMonth()]} ${dueDateObject.getDate()}, ${dueDateObject.getFullYear()}</time>`;

// log this value using console.log
console.log(dueDateString);
